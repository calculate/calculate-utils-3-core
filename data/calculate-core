#!/sbin/openrc-run
# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

depend() {
	after calculate
	after net
	keyword -timeout -docker -lxc -systemd-nspawn -vserver
}

start() {
	ebegin "Start calculate core"

	if ! [[ -f /var/calculate/server/ca/server.key ]]
	then
		eerror "Calculate core is not configured!"
		exit 1
	fi

	start-stop-daemon --background --start --quiet --pidfile /var/run/cl_core.pid \
		--exec /usr/sbin/cl-core -- --pid-file /var/run/cl_core.pid --start
	rm -f /var/lib/calculate/calculate-core/close_on_inactive
	for waiting in {0..24}
	do
		[[ -f /var/run/cl_core.pid ]] && [[ -d /proc/`cat /var/run/cl_core.pid` ]] && break
		sleep 0.5
	done
	[[ -f /var/run/cl_core.pid ]] && [[ -d /proc/`cat /var/run/cl_core.pid` ]]
	eend $?
}

stop(){
	ebegin "Stop calculate core"
	/usr/bin/pkill -2 -f "cl-core.*--start"
	/usr/bin/pkill -f "cl-core.*--start"
	eend 0
}

# vim:ts=4
