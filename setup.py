#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-core

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os, sys
from os import path, system
from glob import glob
import distutils
from distutils.core import setup
from distutils.command import install_data as module_install_data
from distutils.command import install as module_install
from distutils.util import change_root, convert_path


class install(module_install.install):
    user_options = module_install.install.user_options + [
        ("dbus", None, "install dbus service")
    ]
    def initialize_options(self):
        module_install.install.initialize_options(self)
        self.dbus = False
        self.data_files = self.distribution.data_files
        self.outfiles = []
        self.uses = []

    def finalize_options(self):
        module_install.install.finalize_options(self)
        if self.dbus:
            self.uses.append("dbus")
        self.sub_commands = [x for x in self.sub_commands if x[0] != 'install_data']

    def install_data_run(self):
        self.mkpath(self.install_data)
        for f in self.data_files:
            if isinstance(f, str):
                # it's a simple file, so copy it
                f = convert_path(f)
                if self.warn_dir:
                    self.warn("setup script did not provide a directory for "
                              "'%s' -- installing right in '%s'" %
                              (f, self.install_data))
                (out, _) = self.copy_file(f, self.install_data)
                self.outfiles.append(out)
            else:
                # it's a tuple with path to install to and a list of files
                if len(f) > 2:
                    if f[2] not in self.uses:
                        continue
                    
                dir = convert_path(f[0])
                if not os.path.isabs(dir):
                    dir = os.path.join(self.install_data, dir)
                elif self.root:
                    dir = change_root(self.root, dir)
                self.mkpath(dir)

                if f[1] == []:
                    # If there are no files listed, the user must be
                    # trying to create an empty directory, so add the
                    # directory to the list of output files.
                    self.outfiles.append(dir)
                else:
                    # Copy files, adding them to the list of output files.
                    for data in f[1]:
                        # is's a simple filename without chmod
                        if isinstance(data,str):
                            chmod = None
                        else:
                            data, chmod = data
                        data = convert_path(data)
                        (out, _) = self.copy_file(data, dir)
                        if chmod and os.stat(out).st_mode != chmod:
                            os.chmod(out,chmod)
                        self.outfiles.append(out)

    def run (self):
        module_install.install.run(self)
        self.install_data_run()


__version__ = "3.5.7"
__app__ = "calculate-core"


packages = [
    "calculate."+str('.'.join(root.split(os.sep)[1:]))
    for root, dirs, files in os.walk('pym/core')
    if '__init__.py' in files
]

setup(
    name = __app__,
    version = __version__,
    description = "The program for configuring WSDL server",
    author = "Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    data_files = (
        ('/usr/sbin', [('bin/cl-core',0o755)]),
        ('/etc/init.d', [('data/calculate-core',0o755)]),
        ('/usr/share/man/man1',glob('man/cl-*')),
        ('/usr/share/man/ru/man1',glob('man/ru/*')),
        ('/usr/libexec/calculate', [('data/cl-core-wrapper', 0o755)]),
        ('/usr/libexec/calculate', [('data/cl-pkg-cache', 0o755)]),
        ('/usr/libexec/calculate', [('data/cl-check-admin', 0o755)]),
        ('/usr/libexec/calculate', [('data/cl-variable', 0o755)]),
        ('/usr/libexec/calculate', [('data/cl-dbus-core.py', 0o755)], "dbus"),
        ('/usr/share/dbus-1/system.d', [('data/org.calculate.Core.conf', 0o644)], "dbus"),
        ('/usr/share/dbus-1/system-services', [('data/org.calculate.Core.service', 0o644)], "dbus"),
    ),
    package_dir = {'calculate.core': "pym/core"},
    packages = packages,
    cmdclass={'install': install }
)
