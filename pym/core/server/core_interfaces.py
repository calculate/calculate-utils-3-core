# -*- coding: utf-8 -*-

# Copyright 2015-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from calculate.lib.datavars import DataVars
import calculate.contrib
from spyne import Service

class RerunTrigger(Exception):
    pass

class CoreServiceInterface():
    ########
    # Fields
    ########

    ##############
    # baseClass.py
    ##############
    Common = object
    glob_process_dict = None
    process_pid = None
    glob_progress_dict = None
    glob_table_dict = None
    glob_frame_list = None
    manager = None

    data_path = None
    certbase = None
    rights = None
    sids = None
    pids = None
    sids_file = None
    ssl_certificate = None
    ssl_private_key = None
    cachedict = None

    # set_paths
    sids_pids = None
    cert_path = None
    serv_certbase = None
    group_rights = None
    max_sid = None
    max_pid = None
    log_filename = None
    pids_file = None

    #workaround: Spyne won't let us use self ref in @rpc, 
    # and ctx leads to original service class (Basic in this case)
    # but some methods are gathered from CoreWsdl classes, so we need
    # a ref to combined class
    comb_class_ref = None

    ############
    # gen_sid.py
    ############
    sid_locker = None

    #########
    # Methods
    #########

    ##############
    # baseClass.py
    ##############
    def get_cache(sid, meth_name, obj_name):
        """
        Получить значение кэша
        :param sid: id сессии
        :param meth_name: имя метода
        :param obj_name: имя объекта
        :return:
        """
        raise NotImplementedError

    def clear_cache(sid, meth_name=None, obj_name=None):
        """
        Удалить объект из кэша, если не указан метод - очищается весь кэш
        для указанной сессий, если не указано имя объекта - очищается весь
        кэш для метода сессии
        :param sid: id сессии
        :param meth_name: имя метода
        :param obj_name: имя объекта
        :return:
        """
        raise NotImplementedError

    def clear_cache_method(method=None):
        """
        Удалить из всех сессий для указанного метода
        :param meth_name: имя метода
        :return:
        """
        raise NotImplementedError

    def set_cache(sid, meth_name, obj_name, obj, smart=True):
        """
        Установить кэш для метода, если используется smart, то происходит
        корриктровка атрибутов предыдущего объекта
        :param sid: id сессии
        :param meth_name: имя метода
        :param obj_name: имя объекта
        :param obj: кэшируемый объект
        :param smart: корректировка предыдущего значения
        :return:
        """
        raise NotImplementedError

    def set_comb_class_ref(cls, comb_class_ref):
        cls.comb_class_ref = comb_class_ref

    #########
    # func.py
    #########
    def startprocess(cls, sid, target=None, method=None, method_name=None,
                     auto_delete=False, args_proc=()):
        """
        Запустить задачу
        :param sid: id сессии
        :param target: объект с методом
        :param method: метод
        :param method_name: название метода
        :param auto_delete: отслеживание процесса (нигде не используется)
        :param args_proc: параметры для метода
        :return:
        """
        raise NotImplementedError

    def install_vars(cls, dv=None):
        """
        Метод описания параметров метода install (создаётся автоматически
        через метакласс (datavarsConstructor.wrapper)
        :param dv: объект переменных
        :return:
        """
        raise NotImplementedError

    def serv_get_methods(cls, client_type):
        """
        Получить доступные методы
        :param client_type: тип клиента (console или gui или None)
        :return:
        """
        raise NotImplementedError

    def serv_get_sessions(cls):
        """
        Получить текущие сессии
        :return:
        """
        raise NotImplementedError

    def active_clients(cls, sid):
        """
        Зафиксировать активность клиента и проверить рабочая ли сессия
        :param sid: id сессии
        :return:
        """
        raise NotImplementedError

    def serv_view_cert_right(cls, cert_id, data_path, client_type=None):
        """
        Получить права указанного сертификата
        :param cert_id: id сертификата
        :param data_path: база сертификатов
        :param client_type: тип клиента (console или gui, или None)
        :return:
        """
        raise NotImplementedError

    ############
    # gen_pid.py
    ############

    def check_sid_cert(cls, sid):
        """
        Проверить сертификат в указанной сессии
        :param sid: id сессии
        :return:
        """
        raise NotImplementedError

    def find_sid_pid_file(cls, sid):
        """
        Найти id процессов указанной сессии
        :param sid: id сессии
        :return: список pid
        """
        raise NotImplementedError

    def serv_pid_kill(cls, pid, sid):
        """
        Заверишить процесс
        :param pid:
        :param sid:
        :return:
        """
        raise NotImplementedError

    def del_pid(cls, pid):
        """
        Удалить процесс из списка процессов
        :param pid: id процесса
        :return:
        """
        raise NotImplementedError

    def gen_pid(cls):
        """
        Сгенерировать идентификатор процесса
        :return:
        """

    ##########
    # clean.py
    ##########
    def delete_pid(cls, sid, pid):
        """
        Удалить сохранённые данные указанного процесса у сессии
        :param sid: id сессии
        :param pid: id процесса
        :return:
        """
        raise NotImplementedError

    def watcher_pid_proc(cls, sid, pid):
        """
        Функция для потока для наблюдение за процессом сессии
        :param sid: id сессии
        :param pid: id процесса
        :return:
        """

    ###########
    # gen_sid.py
    ###########
    def sid_cmp(cls, sid, cert_id, lang):
        """
        Найти указанную сессию в сервере или создать новую
        :param sid: 0 или id сессии
        :param cert_id: id сертификата
        :param lang: язык
        :return:
        """
        raise NotImplementedError

    def serv_init_session(cls, sid, lang):
        """
        Инициализация сессии
        :param sid: 0 или id сессии
        :param lang: язык
        :return:
        """
        raise NotImplementedError

    def del_sid_from_file(cls, sid):
        """
        Удалить сессию из файла (закрыть сессию)
        :param sid: id сессии
        :return:
        """
        raise NotImplementedError

    def serv_sid_info(cls, sid):
        """
        Получить информацию о сессии
        :param sid: id сессии
        :return:
        """
        raise NotImplementedError

    def find_sid_in_file(cls, sid):
        """
        Найти идентификатор сессии в файле
        :param sid: id сессии
        :return:
        """

    #################
    # sid_pid_file.py
    #################
    def del_sid_pid(cls, sid):
        """
        Удалить сессию и все её процессы
        :param sid: id сессии
        :return:
        """
        raise NotImplementedError

    def serv_pid_info(cls, sid, pid):
        """
        Получить информацию о процессе
        :param sid: id сессии
        :param pid: id процесса
        :return:
        """
        raise NotImplementedError

    def del_pid_from_sid_pid(cls, pid):
        """
        Удалить информацию о процессе из sid_pid файла
        :param pid: id процесса
        :return:
        """
        raise NotImplementedError

    def add_sid_pid(cls, sid, pid):
        """
        Добавить запись соответивия процесса сессии
        :param sid: id сессии
        :param pid: id процесса
        :return:
        """
        raise NotImplementedError


class MethodsInterface():
    clVars = DataVars()
    method_name = None

    def applyTemplates(self, *args, **kw):
        raise NotImplementedError

    def dispatchConf(self, *args, **kw):
        raise NotImplementedError

    def pauseProcess(self):
        raise NotImplementedError

    def resumeProcess(self):
        raise NotImplementedError

    def writeFile(self):
        raise NotImplementedError

    def setProgress(self, perc, short_message=None, long_message=None):
        raise NotImplementedError

    def setStatus(self, stat):
        raise NotImplementedError

    def setData(self, dat):
        raise NotImplementedError

    def getStatus(self):
        raise NotImplementedError

    def getProgress(self):
        raise NotImplementedError

    def getAnswer(self):
        raise NotImplementedError

    def addProgress(self, message=""):
        raise NotImplementedError

    def printTable(self, table_name, head, body, fields=None,
                   onClick=None, addAction=None, step=None,
                   records=None):
        raise NotImplementedError

    def addMessage(self, type='normal', message=None, id=None,
                   onlyShow='', default=None):
        raise NotImplementedError

    def printSUCCESS(self, message='', onlyShow=None):
        raise NotImplementedError

    def printPre(self, message='', onlyShow=None):
        raise NotImplementedError

    def printDefault(self, message='', onlyShow=None):
        raise NotImplementedError

    def printWARNING(self, message, onlyShow=None):
        raise NotImplementedError

    def printERROR(self, message='', onlyShow=None):
        raise NotImplementedError

    def startTask(self, message, progress=False, num=1):
        raise NotImplementedError

    def setTaskNumber(self, number=None):
        raise NotImplementedError

    def endTask(self, result=None, progress_message=None):
        raise NotImplementedError

    def askConfirm(self, message, default="yes"):
        raise NotImplementedError

    def isInteractive(self):
        raise NotImplementedError

    def askChoice(self, message, answers=(("yes", "Yes"), ("no", "No"))):
        raise NotImplementedError

    def askQuestion(self, message):
        raise NotImplementedError

    def askPassword(self, message, twice=False):
        raise NotImplementedError

    def beginFrame(self, message=None):
        raise NotImplementedError

    def endFrame(self):
        raise NotImplementedError

    def startGroup(self, message):
        raise NotImplementedError

    def endGroup(self):
        raise NotImplementedError
