# -*- coding: utf-8 -*-

# Copyright 2021 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

 
import calculate.contrib
from spyne.service import Service, ServiceMeta
from spyne import String, Integer, Array, rpc
from .func import WsdlMeta
from .api_types import ReturnedMessage, ViewInfo, ViewParams
from spyne.protocol._outbase import OutProtocolBase
from .api_types import LazyString
 
#monkey patch:
def to_unicode(self, cls, value, *args, **kwargs):
    if value is None:
        return None

    #### PATCH ####################
    if isinstance(value, LazyString):
        value = str(value)
    ###############################

    handler = self._to_unicode_handlers[cls]
    retval = handler(cls, value, *args, **kwargs)

    return retval

OutProtocolBase.to_unicode = to_unicode


class ServiceMetaAdapter(ServiceMeta):
    #ref to the created class
    global_class_ref = None

#this is used for a localCall	
class CoreInnerWsdl(ServiceMeta, WsdlMeta):
    pass

# Nevermind, following is deprecated:

#the idea is to decorate methods that need rpc with this
#and then find them in make_service and wrap them in funcs without self ref
# def rpc_a():
# 	def func(f):
# 		f.wrap_rpc = True
# 		return f
# 	return func

#wraps a func in rpc decorator
#this is needed because Spyne service does not allow self ref in function calls
def make_rpc_func_view(func_to_call):
    def _function(ctx, sid, params):
        return func_to_call(ServiceMetaAdapter.global_class_ref, sid, params)
    _function.__name__ = func_to_call.__name__
    return rpc(Integer, ViewParams, _returns=ViewInfo)(_function)

def make_rpc_func_vars(func_to_call):
    def _function(ctx, dv=None, params=None):
        return func_to_call(ServiceMetaAdapter.global_class_ref, dv, params)
    _function.__name__ = func_to_call.__name__
    return staticmethod(_function)
                
def make_rpc_func_caller(func_to_call):
    def _function(ctx, sid, params):
        return func_to_call(ServiceMetaAdapter.global_class_ref, sid, params)
    _function.__name__ = func_to_call.__name__
    info_class = func_to_call.info_class
    return rpc(Integer, info_class, _returns=Array(ReturnedMessage))(_function)

def make_service(basic_class, wsdl_core_class_list, outer_wsdl_class_list, service_name):
    #for Core, rpc methods only present in basic_class, we need others just to have a mono class
    #for outer wsdl classes, we have to add stateless wrap methods on our own

    saved_pub_methods = basic_class.public_methods
    functions_to_add = {}
    for klass in outer_wsdl_class_list:
        for meth in klass.__dict__:
            if not meth.startswith("__") and not meth == "methods":
                if meth.endswith("_view"):
                    functions_to_add[meth] = make_rpc_func_view(getattr(klass, meth))
                elif meth.endswith("_vars"):
                    functions_to_add[meth] = make_rpc_func_vars(getattr(klass, meth))
                else:
                    functions_to_add[meth] = make_rpc_func_caller(getattr(klass, meth))
    
    functions_to_add.update({"__metaclass__" : ServiceMetaAdapter})
    ClService = ServiceMetaAdapter(service_name, tuple([basic_class] + [wsdl_core_class_list]), functions_to_add)
    ClService.public_methods.update(saved_pub_methods)
    ServiceMetaAdapter.global_class_ref = ClService
    ClService.set_comb_class_ref(ClService)
    return ClService
