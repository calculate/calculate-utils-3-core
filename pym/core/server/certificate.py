# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from .func import MethodsInterface

from calculate.lib.cl_lang import getLazyLocalTranslate, setLocalTranslate
from calculate.lib.utils.common import getPagesInterval

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Certificate(MethodsInterface):
    """
    Объект работы с сертификатами
    """

    def show_certs_meth(self, page_count, page_offset):
        """
        Отобразить таблицу с сертификатами
        """
        dv = self.clVars
        list_cert_id = dv.Get('cl_list_cert_id')
        try:
            list_cert_id.sort(key=lambda x: int(x))
        except ValueError:
            list_cert_id.sort()

        if not list_cert_id:
            self.printSUCCESS(_("No certificates"))

        head = [_('Certificates'), _('Groups'), _('Permissions')]
        body = []
        fields = ['cl_cert_id', '']

        for cert in list_cert_id[page_offset:page_offset + page_count]:
            dv.Set('cl_cert_id', cert)
            group_rights = ', '.join(dv.Get('cl_cert_perms'))
            cert_groups = ', '.join(dv.Get('cl_cert_groups'))
            body.append([str(cert), cert_groups, group_rights])

        if body:
            self.printTable(_("List of certificates"), head,
                            body, fields=fields,
                            onClick='core_detail_view_cert')
            num_page, count_page = getPagesInterval(
                page_count, page_offset,
                len(list_cert_id))
            self.printSUCCESS(_('page %d from ') % num_page + str(count_page))
        return True
