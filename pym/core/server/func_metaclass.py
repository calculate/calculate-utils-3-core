# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import importlib
import logging
# logger = logging.getLogger(__name__)

list_module = ["clean", "gen_pid",
               "sid_pid_file", "gen_sid", "func", "api_types"]

imported_modules = []
pack = "calculate.core.server"
for module_name in list_module:
    imported_modules.append(importlib.import_module(
        '%s.%s' % (pack, module_name)).CoreWsdl)


imported_modules.append(object)
imported = tuple(imported_modules)

# create metaclass, including all methods server class
Func_MetaClass = type("Func_MetaClass", imported, {})
