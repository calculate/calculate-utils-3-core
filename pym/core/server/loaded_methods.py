# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from collections import defaultdict


class LoadedMethods():
    conMethods = {}
    guiMethods = {}
    rightsMethods = {}
    methodDepends = defaultdict(list)

    # add needed rights for call method
    @classmethod
    def check_permissions(cls, add_right=(), static_method = False):
        def wrapper(function):
            name = function.__func__.__name__ if static_method else function.__name__
            cls.rightsMethods[name] = add_right
            return function

        return wrapper

    # add needed rights for call method
    @classmethod
    def core_method(cls, gui=False, category=None, title=None, image=None,
                    command=None, rights=(), user=False, depends=(), static_method = False):
        def wrapper(function):
            name = function.__func__.__name__ if static_method else function.__name__
            if gui:
                if category not in cls.guiMethods.keys():
                    cls.guiMethods[category] = []
                cls.guiMethods[category].append(name)
                cls.guiMethods[category].append(title)
                cls.guiMethods[category].append(image)
                if callable(gui):
                    cls.guiMethods[category].append(gui)
                else:
                    cls.guiMethods[category].append(None)

            if command:
                if command not in cls.conMethods.keys():
                    cls.conMethods[command] = []
                cls.conMethods[command].append(name)
                cls.conMethods[command].append(user)
                if title:
                    cls.conMethods[command].append(title)
            for depend in depends:
                cls.methodDepends[depend].append(name)

            if rights:
                cls.rightsMethods[name] = rights
                if not name.endswith('_view'):
                    cls.rightsMethods[name + '_view'] = rights
            return function

        return wrapper
