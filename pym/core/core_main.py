#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# from server.cl_server import main
# print(main)

from importlib import reload
def core_main():
    import sys

    if hasattr(sys, "setdefaultencoding"):
        sys.setdefaultencoding("utf-8")
    from calculate.lib.cl_lang import setLocalTranslate

    _ = lambda x: x
    setLocalTranslate('cl_core', sys.modules[__name__])
    from traceback import print_exc
    from os import path

    if not path.exists('/dev/urandom'):
        sys.stderr.write("/dev/urandom not found\n")
        sys.exit(1)

    try:
        from .server.cl_server import main

        reload(sys)
        from calculate.lib.datavars import CriticalError, DataVarsError

        try:
            sys.exit(main())
        except (CriticalError, DataVarsError) as e:
            sys.stderr.write("%s\n" % str(e))
            sys.exit(1)
        except ImportError as e:
            print_exc()
            cannot_import = 'cannot import name '
            no_module = 'No module named '
            se = str(e)
            if se.startswith(cannot_import):
                print (_('Failed to import %s')
                       % se.rpartition(cannot_import)[2])
            elif se.startswith(no_module):
                print (_('No module named %s') %
                       se.rpartition(no_module)[2])
            else:
                print(e)
            sys.exit(1)
    except KeyboardInterrupt:
        print()
        print(_("Task interrupted"))


if (__name__ == "__main__"):
    core_main()