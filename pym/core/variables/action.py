# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import ActionVariable, Variable, VariableError

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_core3', sys.modules[__name__])


class Actions():
    Restore = "restore"
    Service = "service"

    Backup = "backup"
    BackupRestore = "restore"


class VariableAcBackupCreate(ActionVariable):
    """
    Создание архива резвервной копии
    """

    def action(self, cl_action):
        if cl_action == Actions.Backup:
            return "on"
        return "off"


class VariableAcBackupRestore(ActionVariable):
    """
    Пошаговое восстановление сервисов и настроек из архива резервной копии
    """

    def action(self, cl_action):
        if (cl_action == Actions.BackupRestore and self.Get(
                'cl_backup_action') == Actions.Restore):
            return "on"
        return "off"

class VariableAcBackupService(ActionVariable):
    """
    Шаблоны выполняемые после восстановленных конфигурационных файлов
    """

    def action(self, cl_action):
        if (cl_action == Actions.BackupRestore and self.Get(
                'cl_backup_action') == Actions.Service):
            return "on"
        return "off"


class VariableClBackupAction(Variable):
    """
    Подтип действия связанного с резервной копией настроек
    """
    value = ""

    def check(self, value):
        """
        Проверка наличия архива резервных настроек для восстановления
        :param value:
        :return:
        """
        if value in (Actions.Restore, Actions.Service):
            if not self.Get('cl_backup_file'):
                raise VariableError(_("Failed to find backup"))
