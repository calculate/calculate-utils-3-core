# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0 #
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from calculate.lib.datavars import Variable
import sys
from os import path

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_core3', sys.modules[__name__])


class VariableClCoreMonitorPeriod(Variable):
    """
    Variable store period monitoring session
    """
    type = "int"
    value = "30"


class VariableClCoreSidLive(Variable):
    """
    Variable store time session live
    """
    type = "int"
    value = "1440"


class VariableClCoreRights(Variable):
    """
    Variable store path to file with rights
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "conf/right.conf")


class VariableClCoreGroupRightsPath(Variable):
    """
    Variable store path to file with group rights
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "conf/group_right.conf")


class VariableClCoreLocalData(Variable):
    """
    Variable store path to data files
    """
    value = '/var/lib/calculate/calculate-core'


class VariableClCoreData(Variable):
    """
    Variable store path to data files
    """
    value = '/var/calculate/server'


class VariableClCoreClientCertsPath(Variable):
    """
    Переменная хранит путь до клиентских сертификатов
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "client_certs")


class VariableClCoreClientNewIdPath(Variable):
    """
    Переменная хранит путь до счетчика клиентских сертификатов
    """

    def get(self):
        return path.join(self.Get('cl_core_client_certs_path'), "id.int")


class VariableClCoreDatabase(Variable):
    """
    Variable store name files containing clients certificates
    """

    def get(self):
        return path.join(self.Get('cl_core_client_certs_path'),
                         "Database")


class VariableClCoreServDatabase(Variable):
    """
    Variable store name files containing signed servers certificates
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "server_certs/Database")


class VariableClCoreSidsPath(Variable):
    """
    Variable store path to sessions id files
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "sids")


class VariableClCorePidsPath(Variable):
    """
    Variable store path to process id files
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "pids")


class VariableClCoreSidsFile(Variable):
    """
    Variable store name sessions database files
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "sid.db")


class VariableClCorePidsFile(Variable):
    """
    Variable store name process id database files
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "pid.db")


class VariableClCoreSidsPids(Variable):
    """
    Variable store name file comparison sessions and process 
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "sid_pid")


class VariableClCoreMaxSid(Variable):
    """
    Variable store maximum session id
    """
    value = "10000"


class VariableClCoreMaxPid(Variable):
    """
    Variable store maximum process id
    """
    value = "100000"


class VariableClCoreCert(Variable):
    """
    Server certificate
    """

    def get(self):
        return path.join(self.Get('cl_core_cert_path'), "server.crt")


class VariableClCoreCertPath(Variable):
    """
    Server certificate path
    """

    def get(self):
        return path.join(self.Get('cl_core_data'), "ca")


class VariableClCoreKey(Variable):
    """
    Private server key
    """

    def get(self):
        return path.join(self.Get('cl_core_cert_path'), "server.key")


class VariableClCoreCertLive(Variable):
    """
    Max time live sessions (in minutes)
    """
    type = "int"
    value = "1576800"


class VariableClCoreClientActivePeriod(Variable):
    """
    Period client activity (in seconds)
    """
    type = "int"
    value = "15"


class VariableClCoreGetFramePeriod(Variable):
    """
    Period request frames (in seconds)
    """
    type = "int"
    value = "2"


class VariableClClientCertDir(Variable):
    """
    Client certificates directory
    """
    value = '~/.calculate/client_cert/'


class VariableClLogPath(Variable):
    """
    Server log file path certificates directory
    """
    value = '/var/log/calculate'


class VariableClUserRootCert(Variable):
    """
    Trusted certificates added by client
    """

    def get(self):
        return path.join(self.Get('cl_client_cert_dir'), "ca/ca_root.crt")


class VariableClGlobRootCert(Variable):
    """
    Trusted certificates installed in system
    """

    def get(self):
        return path.join(self.Get('cl_core_cert_path'), "sys_ca.crt")


class VariableClCorePort(Variable):
    """
    Port for cl-core WSDL server
    """
    type = "int"
    #was 8888
    value = "2007"


class VariableClCoreRestartPath(Variable):
    """
    Файл-флаг необходимости перезапуска сервера утилит
    """

    def get(self):
        return path.join(self.Get('cl_core_local_data'), "restart")


class VariableClCoreDbusStopPath(Variable):
    """
    Файл-флаг необходимости остонова сервера утилит при отсутствии клиента
    """

    def get(self):
        return path.join(self.Get('cl_core_local_data'), "close_on_inactive")


class VariableClCoreAdminPath(Variable):
    """
    Путь до ini.env сервера, который содержит информацию о локальных
    администраторах
    """
    value_format = "{core.cl_core_data}/ini.env"
