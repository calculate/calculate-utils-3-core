# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


 
import sys
from .function import get_sid

client_types = "console"

_ = lambda x: x
from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_core3', sys.modules[__name__])


def pid_inf(client, sid, pids):
    """ get and show information about process """
    print("============================")
    for pid in pids:
        s = client.service.pid_info(sid, pid)
        if s == "":
            print(_("PID not found"))
            return 1
        if s[0][0] == "Permission denied":
            print(_("Permission denied"))
            return 1

        print('\n' + _(u"Process name: %s") % s[0][3])
        print(_(u"Process ID: %s") % s[0][0])
        if s[0][1] == '1':
            print(_(u"Process active"))
        elif s[0][1] == '0':
            print(_(u"Process completed"))
        else:
            print(_(u"Process killed"))
        print(_(u"%s: Process started") % s[0][2])
    print("============================")
    return 0


def client_list_pid(client):
    """ get all process id for this session """
    sid = get_sid(client.SID_FILE)

    try:
        list_pid = client.service.list_pid(sid=sid)
        if list_pid[0][0] == 0:
            print(_("PIDs not found for this session!"))
            return 0
        else:
            for i in list_pid[0]:
                print("pid - %d" % i)
    except Exception:
        print(_("Failed to get PIDs from the server"))
        return 1
    return len(list_pid[0])


def gen_pid_ls(client, pid_ls):
    """ generation list with pid for this session """
    sid = get_sid(client.SID_FILE)

    try:
        list_pid = client.service.list_pid(sid=sid)
        if list_pid[0][0] == 0:
            print(_("PIDs not found for this session!"))
            return 0
        else:
            for i in list_pid[0]:
                pid_ls.append(i)
    except Exception:
        print(_("Failed to get PIDs from the server"))
        return 0
    return pid_ls


def client_pid_info(client):
    """ get information about selected process (or about all) """
    pid = input(_("PID") + _(": "))
    try:
        pid = int(pid)
    except ValueError:
        print(_("PID error"))
        return 1
    try:
        pid_ls = []
        pid_get = [pid]
        sid = get_sid(client.SID_FILE)
        if pid > 0:
            pid_inf(client, sid, pid_get)
        elif pid == 0:
            if gen_pid_ls(client, pid_ls):
                pid_inf(client, sid, pid_ls)
    except Exception:
        print(_("Failed to get data"))
        return 1
    return 0


def client_list_methods(client):
    """ get & show all available methods for this certificate """
    DAT = 0  # Access to data soap structure
    RES = 0  # Access to result
    COM = 0  # Getting command line
    METH = 1  # Getting method line

    results = client.service.get_methods(client_types)
    if not results:
        print(_('No methods available'))
        return 1
    try:
        if results[DAT][RES][RES][COM] == '0':
            print(_('No methods available'))
            return 1
    except (IndexError, AttributeError):
        pass
    print('\n' + _("You can execute:"))
    for num in range(0, len(results[DAT])):
        print("  %s - %s" % (results[DAT][num][RES][COM],
                             results[DAT][num][RES][METH]))


def client_list_sessions(client):
    """ get all sessions on server """
    results = client.service.get_sessions()
    if results[0][0] == "Permission denied":
        print(results[0][0])
        return 1

    print(_("Sessions running:"))
    for sess in results[0]:
        print(" - %s" % sess)
    return 0


def client_pid_kill(client):
    """ kill process on server """
    pid = input(_("PID to be killed: "))
    try:
        pid = int(pid)
    except ValueError:
        print(_("PID error"))
        return 1

    sid = get_sid(client.SID_FILE)

    result = client.service.pid_kill(pid, sid)
    if result == 0:
        print(_(" Killed successfully!"))
    elif result == 2:
        print(_(" Process completed!"))
    elif result == -1:
        print(_(" Certificate not found in the server database!"))
    elif result == -2:
        print(_(" Session not matching your certificate!"))
    elif result == 1:
        print(_(" Failed to terminate the process!"))
