# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


 
import sys
from .function import get_sid

_ = lambda x: x
from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_core3', sys.modules[__name__])


def client_sid(sid, client, cert_id):
    """ get number session from server and write this in file """
    # lang = input ("Enter language (ru, en, de, fr): ")
    lang = "ru"
    new_sid = client.service.post_sid(sid=sid, cert_id=cert_id, lang=lang)
    fi = open(client.SID_FILE, 'w')
    sid = str(new_sid[0][0])
    fi.write(sid)
    fi.close()
    if new_sid[0][1] == 1:
        print(_(" New session"))
    else:
        print(_(" Old session"))
    print(_(" Your session ID = %s") % sid)


def client_del_sid(client):
    """ delete this session """
    sid = get_sid(client.SID_FILE)
    try:
        s = client.service.del_sid(sid)

        if s[0][0] == "-1":
            print(_("No access to the file!"))
            return -1
        if s[0][0] == "1":
            print(_("Failed to obtain the certificate data!"))
            return -2
        if s[0][0] == "Permission denied":
            print(_("%s: Permission denied") % s[1][1])
            return -3

        if s[0][0] == '0':
            fi = open(client.SID_FILE, 'w')
            fi.write('0')
            fi.close()
            print(_("SID deleted!"))
    except Exception:
        print(_("Failed to delete the SID on the server"))
        return 1
    return 0


def sid_inf(client, sid):
    """ get information about selected session """
    s = client.service.sid_info(sid)
    if s[0][0] == "-1":
        print(_("Session not registered on the server!"))
        return -1
    if s[0][0] == "-2":
        print(_("Failed to obtain the certificate data!"))
        return -2
    if s[0][0] == "Permission denied":
        print(_("%s: Permission denied") % s[0][1])
        return -3

    print("============================")
    print('\n' + _(u"Session number: %s") % sid)
    if s[0][5] == "0":
        print(_(u"Session active"))
    else:
        print(_(u"Session inactive"))
    print(_(u"Certificate number: %s") % s[0][0])
    print(_(u"Certificate issued on %s") % s[0][1])
    print(_(u"IP: %s") % s[0][2])
    print(_(u"MAC: %s") % s[0][3])
    print(_(u"Client type: %s") % s[0][4])
    print("============================")
    return 0


def client_session_info(client):
    """ select session for get information """
    sid = input("SID: ")
    try:
        sid = int(sid)
    except ValueError:
        print(_("Invalid SID"))
        return 1
    try:
        if sid > 0:
            sid_inf(client, sid)
        else:
            print(_("Please enter a correct SID"))
    except Exception:
        print(_("Failed to get data"))
        return 1
    return 0
