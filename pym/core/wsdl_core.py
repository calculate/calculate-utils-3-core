# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

 
import sys

from calculate.lib.datavars import VariableError, DataVarsError

from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)

from . import setup_package
from .server import certificate as certificate
from .server import groups as groups
from .server import request as request
from . import set_vars
from .backup import Backup, BackupError
from .utils.cl_backup import ClBackupAction
from .utils.cl_backup_restore import ClBackupRestoreAction
from .server.func import WsdlBase
from .utils.cl_core_setup import ClCoreSetupAction
from .utils.cl_core_patch import ClCorePatchAction
from .utils.cl_config import ClConfigAction
from .utils.cl_core_dispatch import ClCoreDispatchAction
from .utils.cl_core_view_cert import ClCoreViewCert
from .utils.cl_core_group import (
    ClCoreGroupShow, ClCoreGroupMod, ClCoreGroupAdd, ClCoreGroupDel)
from .utils.cl_core_request import (
    ClCoreRequestShow, ClCoreRequestConfirm, ClCoreRequestDel)
from .utils.cl_core_variables import (ClCoreVariables,
                                                    ClCoreVariablesShow)
from .utils.cl_core_custom import ClCoreCustomAction
from .utils.cl_core_restart import ClCoreRestartAction
from .variables.action import Actions


class Wsdl(WsdlBase):
    methods = [
        #
        # Настройка пакета во время установки (cl-core-setup)
        #
        {
            # идентификатор метода
            'method_name': "core_setup",
            # категория метода
            'category': __('Configuration'),
            # заголовок метода
            'title': __("Configure a Package"),
            # иконка для графической консоли
            'image': 'calculate-core-setup,'
                     'preferences-desktop-default-applications',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-setup',
            # права для запуска метода
            'rights': ['setup_package'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClCoreSetupAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError,
                             setup_package.SetupPackageError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'merge', 'cl_verbose_set': "on"},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Configure a package"),
                                    normal=('cl_core_pkg_name',),
                                    expert=('cl_core_pkg_category',
                                            'cl_core_pkg_version_opt',
                                            'cl_core_pkg_slot_opt',
                                            'cl_core_pkg_path',
                                            'cl_core_arch_machine',
                                            'cl_templates_locate',
                                            'cl_core_pkg_system_set',
                                            'cl_core_pkg_desktop_set',
                                            'cl_core_pkg_root_set',
                                            'cl_verbose_set',
                                            'cl_dispatch_conf'),
                                    next_label=_("Run"))]},
        #
        # Патч исходников пакета (cl-core-patch)
        #
        {
            # идентификатор метода
            'method_name': "core_patch",
            # категория метода
            'category': __('Configuration'),
            # заголовок метода
            'title': __("Patch"),
            # иконка для графической консоли
            'image': None,
            # метода нет в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-core-patch',
            # права для запуска метода
            'rights': ['configure'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClCorePatchAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError,
                             setup_package.SetupPackageError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'patch',
                        'cl_protect_use_set!': 'off'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Configure a package"),
                                    normal=('cl_core_pkg_name',),
                                    expert=('cl_core_pkg_category',
                                            'cl_core_pkg_version',
                                            'cl_core_pkg_slot',
                                            'cl_core_pkg_path',
                                            'cl_core_arch_machine',
                                            'cl_templates_locate',
                                            'cl_verbose_set'),
                                    next_label=_("Run"))]},
        #
        # Обновление конфигурационных файлов (cl-dispatch-conf)
        #
        {
            # идентификатор метода
            'method_name': "core_dispatch",
            # категория метода
            'category': __('Update '),
            # заголовок метода
            'title': __("Update Settings"),
            # иконка для графической консоли
            'image': 'calculate-core-dispatch,edit-find-replace,computer',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-dispatch-conf',
            # права для запуска метода
            'rights': ['configure'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClCoreDispatchAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError,
                             setup_package.SetupPackageError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'dispatch'},
            # описание груп (список лямбда функций)
            'groups': []},
        #
        # Отобразить сертификаты
        #
        {
            # идентификатор метода
            'method_name': "core_view_cert",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Show Certificates"),
            # иконка для графической консоли
            'image': 'calculate-core-view-cert,certificate-server,'
                     'application-certificate',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-view-cert',
            # права для запуска метода
            'rights': ['certificates'],
            # объект содержащий модули для действия
            'logic': {'Certificate': certificate.Certificate},
            # описание действия
            'action': ClCoreViewCert,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {
                'cl_page_max!': lambda dv: len(dv.Get('cl_list_cert_id'))},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Certificates"),
                                    normal=('cl_page_count', 'cl_page_offset'),
                                    next_label=_("Next"))]},
        #
        # Отобразить детали сертификата
        #
        {
            # идентификатор метода
            'method_name': "core_detail_view_cert",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Certificate Details"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # права для запуска метода
            'rights': ['certificates'],
            # объект содержащий модули для действия
            'logic': {},
            # описание действия
            'action': None,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Certificate details"),
                                    normal=('cl_cert_id', 'cl_cert_groups',
                                            'cl_cert_perms'),
                                    custom_buttons=[('but0', _("Back"),
                                                     "core_view_cert",
                                                     "button")])]},
        #
        # Группы
        #
        {
            # идентификатор метода
            'method_name': "core_group_show",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Show Groups"),
            # иконка для графической консоли
            'image': 'calculate-core-group-show,user-group-properties,'
                     'view-certificate-import,application-certificate',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-group-show',
            # права для запуска метода
            'rights': ['core_group'],
            # объект содержащий модули для действия
            'logic': {'Groups': groups.Groups},
            # описание действия
            'action': ClCoreGroupShow,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {
                'cl_page_max!': lambda dv: len(dv.Choice('cl_core_group'))},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Groups"),
                                    normal=('cl_page_count', 'cl_page_offset'),
                                    next_label=_("Next"))]},
        #
        # Отобразить детали группы
        #
        {
            # идентификатор метода
            'method_name': "core_detail_group",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Group Details"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # права для запуска метода
            'rights': ['core_group'],
            # объект содержащий модули для действия
            'logic': {},
            # описание действия
            'action': None,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Group details"),
                                    normal=(
                                        'cl_core_group',
                                        'cl_core_group_rights'),
                                    custom_buttons=[('but0', _("Back"),
                                                     "core_group_show",
                                                     "button"),
                                                    ('but1', _("Change"),
                                                     "core_group_mod",
                                                     "button"),
                                                    ('but2', _("Delete"),
                                                     "core_group_del",
                                                     "button")])]},
        #
        # Изменить группу
        #
        {
            # идентификатор метода
            'method_name': "core_group_mod",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Modify Group"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-groupmod',
            # права для запуска метода
            'rights': ['core_group'],
            # объект содержащий модули для действия
            'logic': {'Groups': groups.Groups},
            # описание действия
            'action': ClCoreGroupMod,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'modify'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Modify group"),
                                    normal=(
                                        'cl_core_group',
                                        'cl_core_group_rights'),
                                    next_label=_("Done"),
                                    custom_buttons=[('but2', _("Confirm"),
                                                     'core_change_group',
                                                     "button")])]},
        #
        # Добавить группу
        #
        {
            # идентификатор метода
            'method_name': "core_group_add",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Add a Group"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-groupadd',
            # права для запуска метода
            'rights': ['core_group'],
            # объект содержащий модули для действия
            'logic': {'Groups': groups.Groups},
            # описание действия
            'action': ClCoreGroupAdd,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'add'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Add a group"),
                                    normal=(
                                        'cl_core_group',
                                        'cl_core_group_rights'),
                                    next_label=_("Add"))]},
        #
        # Удалить группу
        #
        {
            # идентификатор метода
            'method_name': "core_group_del",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Delete the Group"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-groupdel',
            # права для запуска метода
            'rights': ['core_group'],
            # объект содержащий модули для действия
            'logic': {'Groups': groups.Groups},
            # описание действия
            'action': ClCoreGroupDel,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'delete'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Delete the group"),
                                    normal=('cl_core_group',),
                                    next_label=_("Delete"))]},
        #
        # Запрос на сертификат
        #
        {
            # идентификатор метода
            'method_name': "core_request_show",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Show Requests"),
            # иконка для графической консоли
            'image': 'calculate-core-request-show,view-certificate-import,'
                     'application-certificate',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-request-show',
            # права для запуска метода
            'rights': ['request'],
            # объект содержащий модули для действия
            'logic': {'Request': request.Request},
            # описание действия
            'action': ClCoreRequestShow,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {
                'cl_page_max!': lambda dv: len(dv.Get('cl_list_req_id'))},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Show requests"),
                                    normal=('cl_page_count', 'cl_page_offset'),
                                    next_label=_("Next"))]},
        #
        # Отобразить детали запроса
        #
        {
            # идентификатор метода
            'method_name': "core_detail_request",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Request Details"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # права для запуска метода
            'rights': ['request'],
            # объект содержащий модули для действия
            'logic': {},
            # описание действия
            'action': None,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Group details"),
                                    normal=('cl_req_id', 'cl_req_user_name',
                                            'cl_req_ip',
                                            'cl_req_mac', 'cl_req_date',
                                            'cl_req_location',
                                            'cl_req_group'),
                                    custom_buttons=[('but0', _("Back"),
                                                     "core_request_show",
                                                     "button"),
                                                    ('but1', _("Confirm"),
                                                     "core_request_confirm",
                                                     "button"),
                                                    ('but2', _("Delete"),
                                                     "core_request_del",
                                                     "button")])]},
        #
        # Удалить запрос
        #
        {
            # идентификатор метода
            'method_name': "core_request_del",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Delete the Request"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-request-del',
            # права для запуска метода
            'rights': ['request'],
            # объект содержащий модули для действия
            'logic': {'Request': request.Request},
            # описание действия
            'action': ClCoreRequestDel,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'delete'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Delete the request"),
                                    normal=('cl_req_id',),
                                    next_label=_("Delete"))]},
        #
        # Подтвердить запрос
        #
        {
            # идентификатор метода
            'method_name': "core_request_confirm",
            # категория метода
            # 'category':__('Utilities'),
            # заголовок метода
            'title': __("Confirm the Request"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-request-confirm',
            # права для запуска метода
            'rights': ['request'],
            # объект содержащий модули для действия
            'logic': {'Request': request.Request},
            # описание действия
            'action': ClCoreRequestConfirm,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'confirm'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Delete the request"),
                                    normal=('cl_req_id', 'cl_req_group'),
                                    next_label=_("Delete"))]},
        #
        # установить переменные
        #
        {
            # идентификатор метода
            'method_name': "core_variables",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Setup Variables"),
            # иконка для графической консоли
            'image': 'calculate-core-variables,applications-versioncontrol,'
                     'text-x-preview,text-x-makefile',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-variables',
            # права для запуска метода
            'rights': ['setup_variables'],
            # объект содержащий модули для действия
            'logic': {'Variables': set_vars.Variables},
            # описание действия
            'action': ClCoreVariables,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Setup variables"),
                                    normal=('cl_variable_data',),
                                    next_label=_("Save"))]},
        #
        # отобразить переменные
        #
        {
            # идентификатор метода
            'method_name': "core_variables_show",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("View Variables"),
            # иконка для графической консоли
            'image': None,
            # метод в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-core-variables-show',
            # права для запуска метода
            'rights': ['configure'],
            # объект содержащий модули для действия
            'logic': {'Variables': set_vars.Variables},
            # описание действия
            'action': ClCoreVariablesShow,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Setup variables"),
                                    normal=(
                                        'cl_variable_filter',
                                        'cl_variable_show'),
                                    next_label=_("Show"))]},
        #
        # Выполнить настройку пакета (cl-config)
        #
        {
            # идентификатор метода
            'method_name': "core_config",
            # категория метода
            'category': __('Configuration'),
            # заголовок метода
            'title': __("Config"),
            # иконка для графической консоли
            'image': None,
            # метода нет в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-config',
            # права для запуска метода
            'rights': ['configure'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClConfigAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError,
                             setup_package.SetupPackageError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'config', 'cl_verbose_set': "on"},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Configure a package"),
                                    normal=('cl_core_pkg_name',),
                                    expert=('cl_core_pkg_category',
                                            'cl_core_pkg_version_opt',
                                            'cl_core_pkg_slot_opt',
                                            'cl_templates_locate',
                                            'cl_verbose_set'),
                                    next_label=_("Run"))]},
        #
        # отобразить переменные
        #
        {
            # идентификатор метода
            'method_name': "core_custom",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Custom Action"),
            # иконка для графической консоли
            'image': 'calculate-core-custom,gnome-desktop-config,desktop-config',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-custom',
            # права для запуска метода
            'rights': ['custom_configure'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClCoreCustomAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_verbose_set': "on", 'cl_human_edit_set': "on"},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Custom action"),
                                    normal=(
                                        'ac_custom_name', 'cl_human_edit_set',
                                        'cl_verbose_set'),
                                    expert=(
                                        'ur_core_login', 'cl_core_arch_machine',
                                        'cl_templates_locate',
                                        'cl_dispatch_conf'),
                                    next_label=_("Run"))]},
        #
        # перезапустить сервис calculate core
        #
        {
            # идентификатор метода
            'method_name': "core_restart",
            # категория метода
            'category': __('Utilities'),
            # заголовок метода
            'title': __("Restart calculate-core"),
            # иконка для графической консоли
            'image': 'calculate-core-restart,view-refresh',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-core-restart',
            # права для запуска метода
            'rights': ['core_restart'],
            # объект содержащий модули для действия
            'logic': {'UpdateConfigs': setup_package.UpdateConfigs},
            # описание действия
            'action': ClCoreRestartAction,
            # объект переменных
            'datavars': "core",
            'native_error': (VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'restart'},
            # описание груп (список лямбда функций)
            'groups': []},
        #
        # создание резервной копии настроек
        #
        {
            # идентификатор метода
            'method_name': "backup",
            # категория метода
            'category':__('Backup'),
            # заголовок метода
            'title': __("Backup"),
            # иконка для графической консоли
            'image': 'calculate-backup',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-backup',
            # права для запуска метода
            'rights': ['backup'],
            # объект содержащий модули для действия
            'logic': {'Backup': Backup},
            # описание действия
            'action': ClBackupAction,
            # объект переменных
            'datavars': "core",
            'native_error': (BackupError, VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': Actions.Backup},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("System backup"),
                                    normal=('cl_backup_verbose_set',),
                                    next_label=_("Run"))]},
        #
        # восстановление настроек из резервной копии
        #
        {
            # идентификатор метода
            'method_name': "backup_restore",
            # категория метода
            'category':__('Backup'),
            # заголовок метода
            'title': __("Restore"),
            # иконка для графической консоли
            'image': 'calculate-backup-restore',
            # метод в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-backup-restore',
            # права для запуска метода
            'rights': ['backup'],
            # объект содержащий модули для действия
            'logic': {'Backup': Backup},
            # описание действия
            'action': ClBackupRestoreAction,
            # объект переменных
            'datavars': "core",
            'native_error': (BackupError, VariableError, DataVarsError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': Actions.BackupRestore,
                        'core.cl_backup_action': Actions.Restore},
            # описание груп (список лямбда функций)
            'groups': [
            lambda group: group(_("System restore"),
                                normal=('cl_backup_verbose_set',),
                                brief=('cl_backup_file', 'cl_backup_time'),
                                next_label=_("Run"))],
            'brief': {'next': __("Run"),
                      'name': __("System restore")}},
    ]
