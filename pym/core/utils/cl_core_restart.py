# -*- coding: utf-8 -*-

# Copyright 2013-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.datavars import VariableError
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClCoreRestartAction(Action):
    """
    Действие для настройки параметров видео
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, TemplatesError, VariableError)

    successMessage = __("Action successfully completed!")
    failedMessage = __("Failed to perform action!")
    interruptMessage = __("Action manually interrupted")

    def __init__(self):
        # список задач для действия
        self.tasks = [
            {'name': 'check_running',
             # проверить запущенные процессы
             'method': 'UpdateConfigs.checkRunning()',
             },
            {'name': 'restart',
             # перезапустить calculate-core
             'message': _("Restarting calculate-core"),
             'method': 'UpdateConfigs.restartService("calculate-core")',
             }]

        Action.__init__(self)
