# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError, DataVarsError
from ..backup import BackupError
from calculate.lib.cl_template import TemplatesError
from tarfile import ReadError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClBackupRestoreAction(Action):
    """
    Восстановление настроек из резервной копии
    """
    # ошибки, которые отображаются без подробностей
    native_error = (BackupError, FilesError, DataVarsError, VariableError,
                    TemplatesError, ReadError)
    successMessage = __("Files successfully restored from backup!")
    failedMessage = __("Failed to restore from backup!")
    interruptMessage = __("Restoration from backup manually interrupted")

    restore_tasks = []

    tasks = [
        {'name': 'unpack_backup',
         'message': __("Unpacking backup"),
         'method': 'Backup.open_archive(core.cl_backup_path,'
                   'core.cl_backup_file)'
         },
        {'name': 'restore_accounts',
         'message': __("Restoring user accounts"),
         'method': 'Backup.restore_accounts(core.cl_backup_path)'
        },
        {'name': 'restore_network',
         'message': __("Restoring network"),
         'method': 'Backup.restore_network(core.cl_backup_path)'
         },
        {'name': 'special_restore',
         'method': 'Backup.special_restore(core.cl_backup_path)'
         },
        {'name': 'templates',
         'message': __("Restoring services"),
         'method': 'Backup.applyTemplates(install.cl_source,'
                   'False,True,None,True,True)',
         },
        {'name': 'clear_autorun',
         'method': 'Backup.clear_autorun()',
         },
        {'name': 'restore_configs',
         'message': __("Unpacking configuration files"),
         'method': 'Backup.restore_configs(core.cl_backup_file,"/",'
                   'core.cl_backup_contents_name,core.cl_backup_root_name)'
         },
        {'name': 'restore_content',
         'message': __("Restoring file owners"),
         'method': 'Backup.restore_contents(core.cl_backup_file_contents,'
                   '"/")',
         },
        {'name': 'display_verbose',
         'method': 'Backup.display_changed_configs()',
         'condition': lambda Get: Get('core.cl_backup_verbose_set') == 'on'
         },
        {'name': 'set_service_mode',
         'method': 'Backup.set_service_action()',
         },
        {'name': 'templates_service',
         'message': __("Configuration after restoring from backup"),
         'method': 'Backup.applyTemplates(install.cl_source,'
                   'False,True,None,True,True)',
         },
        {'name': 'remove_dir',
         'method': 'Backup.remove_directory(core.cl_backup_path)',
         'depend': Tasks.success_one_of("unpack_backup")
         },
        {'name': 'dispatch_conf',
         'message': __("Updating configuration files"),
         'method': 'Backup.dispatchConf()',
         'condition': lambda Get: Get('cl_dispatch_conf') != 'skip'
         },
        {'name': 'openrc_default',
         'message': __("Running stopped services"),
         'method': 'Backup.run_openrc("default")',
         }
    ]
