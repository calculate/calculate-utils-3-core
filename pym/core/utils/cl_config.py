# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError, DataVarsError
from calculate.lib.cl_template import TemplatesError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClConfigAction(Action):
    """
    Действие настройка пакета для пользователя и системы
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, TemplatesError, VariableError, DataVarsError)
    templateTaskMessage = __("The system is being configured")
    successMessage = None
    failedMessage = __("Failed to configure the system!")
    interruptMessage = __("Configuration manually interrupted")

    tasks = [
        {'name': 'process_config',
         # наложить шаблоны настройки пакета
         'method': 'UpdateConfigs.processConfig(cl_core_pkg_name,'
                   'cl_core_pkg_version,cl_core_pkg_slot,'
                   'cl_core_pkg_category,cl_verbose_set,cl_dispatch_conf,'
                   'cl_template_path_use,cl_ebuild_phase,'
                   'cl_template_clt_set,cl_core_arch_machine)',
         },
    ]
