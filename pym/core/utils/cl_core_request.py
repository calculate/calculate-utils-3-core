# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClCoreRequestShow(Action):
    """
    Отображение запросов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = None
    failedMessage = None
    interruptMessage = __("Viewing manually interrupted")

    tasks = [
        {'name': 'view_req',
         'method': 'Request.show_request_meth(cl_page_count,cl_page_offset)'
         }]


class ClCoreRequestConfirm(Action):
    """
    Подтверждение запроса
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = __("Certificate {cl_req_crt_path} is signed")
    failedMessage = __("Failed to sign {cl_req_csr_path} request")
    interruptMessage = __("Signing manually interrupted")

    tasks = [
        {'name': 'mod_req',
         'method': 'Request.confirm_request_meth(cl_core_client_certs_path,'
                   'cl_core_cert_path,cl_req_csr_path,cl_req_crt_path,'
                   'cl_req_group)'
         }]


class ClCoreRequestDel(Action):
    """
    Удаление запроса
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = None
    failedMessage = __("Failed to delete the request with ID={cl_req_id}")
    interruptMessage = __("Deleting manually interrupted")

    tasks = [
        {'name': 'del_req',
         'method': 'Request.del_request_meth(cl_core_database,cl_req_csr_path,'
                   'cl_req_crt_path,cl_req_id)'
         }]
