# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClCoreGroupShow(Action):
    """
    Отображение групп
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = None
    failedMessage = None
    interruptMessage = __("Viewing manually interrupted")

    tasks = [
        {'name': 'view_group',
         'method': 'Groups.show_groups_meth(cl_page_count,cl_page_offset)'
         }]


class ClCoreGroupMod(Action):
    """
    Изменение группы
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = __("Group {cl_core_group} changed")
    failedMessage = __("Failed to change {cl_core_group} group")
    interruptMessage = __("Modifying manually interrupted")

    tasks = [
        {'name': 'mod_group',
         'method': (
             'Groups.change_group_meth(cl_core_group,cl_core_group_rights,'
             'cl_core_group_rights_path)')
         }]


class ClCoreGroupAdd(Action):
    """
    Добавление группы
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = __("Group {cl_core_group} added")
    failedMessage = __("Failed to add {cl_core_group} group")
    interruptMessage = __("Adding manually interrupted")

    tasks = [
        {'name': 'add_group',
         'method': 'Groups.add_group_meth(cl_core_group,cl_core_group_rights,'
                   'cl_core_group_rights_path)'
         }]


class ClCoreGroupDel(Action):
    """
    Удаление группы
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,)
    successMessage = __("Group {cl_core_group} deleted")
    failedMessage = __("Failed to delete {cl_core_group} group")
    interruptMessage = __("Deleting manually interrupted")

    tasks = [
        {'name': 'del_group',
         'method': 'Groups.del_group_meth(cl_core_group,'
                   'cl_core_group_rights_path)'
         }]
