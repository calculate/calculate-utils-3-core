# -*- coding: utf-8 -*-

# Copyright 2011-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError, DataVarsError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClCoreVariables(Action):
    """
    Отображение сертификатов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, DataVarsError, VariableError)
    successMessage = None
    failedMessage = None
    interruptMessage = __("Modification of variables manually interrupted")

    tasks = [
        {'name': 'write_vars',
         # записать переменные
         'method': 'Variables.writeVariables(cl_variable_data)'
         }
    ]


class ClCoreVariablesShow(Action):
    """
    Отображение сертификатов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, DataVarsError, VariableError)
    successMessage = None
    failedMessage = None
    interruptMessage = __("Viewing manually interrupted")

    tasks = [
        {'name': 'view_vars',
         # отобразить переменные
         'method': 'Variables.showVariables(cl_variable_show,'
                   'cl_variable_filter,cl_variable_data)'
         }
    ]
