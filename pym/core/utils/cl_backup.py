# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from ..server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError, DataVarsError
from ..backup import BackupError
from calculate.lib.cl_template import TemplatesError

_ = lambda x: x
setLocalTranslate('cl_core3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClBackupAction(Action):
    """
    Создание резервной копии настроек
    """
    # ошибки, которые отображаются без подробностей
    native_error = (BackupError, FilesError, DataVarsError, VariableError,
                    TemplatesError)
    successMessage = __("Backup successfully completed!")
    failedMessage = __("Failed to perform backup!")
    interruptMessage = __("Backup manually interrupted")

    tasks = [
        {'name': 'prepare_dir',
         'method': 'Backup.prepare_backup(core.cl_backup_path,'
                   'core.cl_backup_root_name)'
         },
        {'name': 'backup_marked',
         'message': __("Backing up files configured by templates"),
         'method': 'Backup.backup_marked("/",core.cl_backup_path,'
                   '"etc",core.cl_backup_root_name)'
         },
        {'name': 'templates',
         'message': __("Templates preparing for backup"),
         'method': 'Backup.applyTemplates(install.cl_source,'
                   'False,True,None,True,True)',
         },
        {'name': 'special_backup',
         'method': 'Backup.special_backup(core.cl_backup_path)'
        },
        {'name': 'accounts_backup',
         'message': __("Backing up accounts info"),
         'method': 'Backup.save_accounts(core.cl_backup_path)'
         },
        {'name': 'prepare_content',
         'message': __("Calculating checksums"),
         'method': 'Backup.prepare_contents(core.cl_backup_path,'
                   'core.cl_backup_file_contents,core.cl_backup_root_name)',
         },
        {'name': 'save_initd',
         'method': 'Backup.save_initd(core.cl_backup_path,'
                   'core.cl_backup_root_name)',
         },
        {'name': 'pack_backup',
         'message': __("Packing backup"),
         'method': 'Backup.create_archive(core.cl_backup_path,'
                   'core.cl_backup_file)'
         },
        {'name': 'remove_dir',
         'message': __("Clearing temporary files"),
         'method': 'Backup.remove_directory(core.cl_backup_path)',
         'depend': Tasks.success_one_of("prepare_dir")
         },
        {'name': 'display_verbose',
         'method': 'Backup.display_backup_configs(core.cl_backup_file)',
         'condition': lambda Get: Get('core.cl_backup_verbose_set') == 'on'
         },
        {'name': 'display_arch',
         'message': __("Archive created: {core.cl_backup_file}")
         }
    ]

