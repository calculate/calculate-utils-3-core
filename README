AUTHOR: Calculate Ltd. <support@calculate.ru> 

INSTALL
-------

calculate-api needs the following library version installed, in order to run:
    python >= 2.6
    soaplib = 2.13

    calculate-lib >= 3.0.0_beta2.0

To install calculate-api, just execute the install script 'setup.py'.
Example:

    ./setup.py install

####################################

Create root certificate (self-signed)

$ openssl genrsa -out root.key 1024
$ openssl req -new -key root.key -out root.csr
$ openssl x509 -req -days 1096 -in root.csr -signkey root.key -out root.crt

or

$ openssl req -new -newkey rsa:1024 -nodes -keyout root.key -x509 -days 1096 
    -subj /C=RU/ST=Spb/L=Spb/O=calculate-linux\ \
    Inc/OU=Sale/CN=www.calculate-linux.ru/emailAddress=support@calculate-linux.ru 
    -out root.crt  # CN - unique name root certificate

####################################

Create server certificate

$ openssl genrsa -out server.key 1024
$ openssl req -new -key server.key -out server.csr
$ openssl x509 -req -days 365 -CA root.crt -CAkey root.key
    -CAcreateserial -in server.csr -out server.crt

e.g.
$ cd /var/calculate/server/server_certs/
$ openssl x509 -req -days 365 -CA /var/calculate/server/ca/root.crt -CAkey \
  /var/calculate/server/ca/root.key -CAcreateserial -in ./NUM.csr -out ./NUM.crt
  
NUM - number certificate

####################################

Show certificate

openssl x509 -noout -text -in certificate.crt

Show request

openssl req -noout -text -in request.csr

Show key

openssl rsa -noout -text -in key.key

Show CRL

openssl crl -text -noout -in ca.crl

####################################

Create not root ca certificate

$ openssl genrsa -out ca.key 1024
$ openssl req -new -key ca.key -out ca.csr
$ openssl x509 -req -days 365 -CA root.crt -CAkey root.key -CAcreateserial \
     -extfile ssl-server-ca.cfg -extensions ssl_server_ca -in ca.csr -out ca.crt

$ cat ssl-server-ca.cfg 

[ ssl_server_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always
basicConstraints = critical,CA:true
#basicConstraints = CA:true
keyUsage = keyCertSign, digitalSignature, keyEncipherment, cRLSign
extendedKeyUsage = serverAuth

####################################

Files

/var/calculate/server - directory with clients and servers request and signed 
certificates, rights certificates (groups):
  /var/calculate/server/ca - directory with root certificates, server certificates and 
                             CA certificate.
  /var/calculate/server/client_certs - clients request and signed certificates
  /var/calculate/server/server_certs - servers request and signed certificates
  /var/calculate/server/conf/group_right.conf - list of methods available for groups
  example of content:
      manager pid_info,list_pid,cl_template,cl_login
      
  /var/calculate/server/conf/right.conf - list of number certificates who are allowed 
                                          execution of the method
  example of content:
      pid_info 2 -1
  certificate number 2 has right to run method "pid_info", and the certificate 
  number 1 does not have rights. Use key --right-add and --right-del. See man.